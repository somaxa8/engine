package com.somaxa8.engine.scenes.main

import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.ScreenUtils


class MainScene : Screen {

    private val _assets = MainAssets()
    private val _batch = SpriteBatch()

    init {
        _assets.load()
    }

    override fun show() {

    }

    override fun render(delta: Float) {
        if (_assets.update()) {
            ScreenUtils.clear(1f, 0f, 0f, 1f)
            _batch.begin()

            _batch.draw(_assets.getBadLogic(), 0f, 0f)

            _batch.end()
        }
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun pause() {
    }

    override fun resume() {
    }

    override fun hide() {
    }

    override fun dispose() {
        _batch.dispose()
        _assets.dispose()
    }

}