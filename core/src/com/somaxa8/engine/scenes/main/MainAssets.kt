package com.somaxa8.engine.scenes.main

import com.somaxa8.engine.utilities.AssetLoader
import com.somaxa8.engine.utilities.Loadable
import com.badlogic.gdx.graphics.Texture
import ktx.assets.load
import ktx.assets.getAsset

class MainAssets : AssetLoader(), Loadable {

    override fun load() {
        assetManager.load<Texture>("badlogic.jpg")
    }

    fun getBadLogic(): Texture {
        return assetManager.getAsset("badlogic.jpg")
    }

}