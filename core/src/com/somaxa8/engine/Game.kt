package com.somaxa8.engine

import com.somaxa8.engine.scenes.main.MainScene
import com.somaxa8.engine.utilities.SceneManager
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx

class Game : ApplicationAdapter() {

    companion object {
        lateinit var sceneManager: SceneManager
    }

    override fun create() {
        sceneManager = SceneManager()
        sceneManager.setState(MainScene())
    }

    override fun render() {
        sceneManager.update(Gdx.graphics.deltaTime)
    }

    override fun dispose() {
        sceneManager.dispose()
    }

    override fun resize(width: Int, height: Int) {
        sceneManager.resize(width, height)
    }
}
