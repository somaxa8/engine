package com.somaxa8.engine.utilities

interface Loadable {

    fun load()

}