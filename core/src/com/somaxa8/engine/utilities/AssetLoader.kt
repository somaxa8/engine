package com.somaxa8.engine.utilities

import com.badlogic.gdx.assets.AssetManager

abstract class AssetLoader : Disposable {

    val assetManager = AssetManager()

    fun update(): Boolean {
        return assetManager.update()
    }

    override fun dispose() {
        assetManager.dispose()
    }

}