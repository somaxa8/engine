package com.somaxa8.engine.utilities

import com.badlogic.gdx.Screen

class SceneManager : Updatable, Disposable {

    private var _currentState: Screen? = null

    fun setState(screen: Screen) {
        _currentState?.dispose()
        _currentState = screen
    }

    override fun update(delta: Float) {
        _currentState!!.render(delta)
    }

    fun resize(width: Int, height: Int) {
        _currentState!!.resize(width, height)
    }

    override fun dispose() {
        _currentState!!.dispose()
    }

}
