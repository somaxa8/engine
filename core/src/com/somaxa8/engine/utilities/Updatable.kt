package com.somaxa8.engine.utilities

interface Updatable {

    fun update(delta: Float)

}