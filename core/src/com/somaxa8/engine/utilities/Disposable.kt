package com.somaxa8.engine.utilities

interface Disposable {

    fun dispose()

}